---
layout: secure_and_protect_direction
title: "Category Direction - Dynamic Application Security Testing"
description: "Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. Learn more here!"
canonical_path: "/direction/secure/dynamic-analysis/dast/"
---

- TOC
{:toc}
## Secure

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-03-04` |

## Description
### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

Thank you for visiting this category direction page on Dynamic Application Security Testing (DAST) at GitLab. This page belongs to the Dynamic Analysis group of the Secure stage and is maintained by Derek Ferguson ([dferguson@gitlab.com](mailto:<dferguson@gitlab.com>)).

This direction page is a work in progress and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/2912) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for DAST, we'd especially love to hear from you.

### Overview

Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. 

When an application has been deployed and started, DAST connects to the published service via its standard web port and performs a scan of the entire application. It can enumerate pages and verify if well-known attack techniques, like cross-site scripts or SQL injections are possible. DAST will also scan REST APIs using an OpenAPI specification as a guide for testing the API endpoints.

DAST doesn't need to be language specific, since the tool emulates a web client interacting with the application, as an end user would.

As DAST has historically been the domain of security teams, rather than developers, testing the application in its running state has often been overlooked during the development process. Since this type of testing requires an application to be deployed, the attack surface of the running application is usually not evaluated until the application has finished the development cycle and is deployed to a staging server (or worse, to production!). Testing this late in the SDLC means that developers have little to no time to respond to any vulnerabilities are found and trade-offs must be made between fixing vulnerabilities and releasing on time. This can lead to vulnerabilities being released into production either as a calculated risk or with no knowledge of the vulnerability at all. 

We see the area of dynamic application security testing as an ideal collaboration point between established security teams and developers, leading to finding and fixing vulnerabilities earlier in the SDLC and reducing the number of vulnerabilities released to production. By integrating DAST into their pipelines and utilizing review apps, developers can be more conscious of the security impacts their code has on the running application. This awareness can enable them to take initiative and fix these before merging their features into the default branch. For the security team member, being able to create issues for any vulnerability found earlier in the SDLC (either by reviewing pipeline results or running on-demand scans) allows them to take a proactive approach to security, rather than reactive. All of this allows these teams to work together to reduce the overall risk of deploying new code to a production application.

### Goal

Our goal is to provide DAST as part of the standard development process. This means that DAST is executed every time a new commit is pushed to a branch. We also include DAST as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Since DAST requires a running application, we can provide results for feature branches leveraging [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), temporary environments that run the modified version of the application. We can also provide results for applications running on other servers, such as staging or development environments, either through a CI/CD pipeline scan or a manually triggered on-demand scan.

DAST results can be consumed in the merge request, where new vulnerabilities are shown. A full report is available in the pipeline details page.

DAST results are also a part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status of their projects.

We also want to ensure that the production environment is always secure by allowing users to run an on-demand DAST scan on a deployed app even if there is no change in the code. On-demand scans will allow for out-of-band security testing and issue reproduction, without needing any code changes or merge requests to start a scan.

### Roadmap

- [On-demand DAST scans (MVC - shipped, GA - 13.10)](https://gitlab.com/groups/gitlab-org/-/epics/3053)
  - [Site validation - shipped in 13.8](https://gitlab.com/groups/gitlab-org/-/epics/4871)
  - [Saved on-demand scans - shipped in 13.9](https://gitlab.com/gitlab-org/gitlab/-/issues/287579)
  - [On-demand Site profile additional configuration options](https://gitlab.com/groups/gitlab-org/-/epics/3771)
  - [On-demand branch selection](https://gitlab.com/groups/gitlab-org/-/epics/4847)
- [Aggregation of noisy DAST vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/254043)
- [Peach API scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
- [Browserker scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)
- [CI pipeline scan UI configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/276957)

## What's Next & Why

Now that the on-demand scan MVC has shipped, we want to improve the DAST configuration process by adding more options for configuration and moving most configuration to the GitLab UI, leading to a GA of on-demand DAST scans in 13.7. There are several initiatives in process to enable this by improving the backend configuration and adding the UI configuration options.  The UI configuration options will be available as profiles that can be mixed and matched to make it easier to run different types of DAST scans against a single project. Providing the configurations as profiles will make it easy for a single project to have both targeted and full scans, API and site scans, and passive and active scans all available to run with a couple of clicks.
- [DAST Site validation - shipped in 13.8](https://gitlab.com/groups/gitlab-org/-/epics/4079)
- [DAST Scanner configuration profiles - shipped in 13.9](https://gitlab.com/groups/gitlab-org/-/epics/3720)
- [DAST Site configuration profiles](https://gitlab.com/groups/gitlab-org/-/epics/3789)
- [DAST backend configuration improvements epic](https://gitlab.com/groups/gitlab-org/-/epics/3019)

In addition to using profiles to quickly configure scans, we will also be enabling users to save their on-demand scans so commonly run scans can be initiated with a single click. Adding onto the saved scans, we will also give users the ability to associate their scans with branches other than the default branch. Since the default branch is typically a protected branch and not every developer or security team member has authorization to run scans on it, being able to select which branch to associate the scans results with will allow for a wider range of users to leverage on-demand scans.
- [Saved on-demand scans - shipped in 13.9](https://gitlab.com/gitlab-org/gitlab/-/issues/287579)
- [DAST On-demand branch selection](https://gitlab.com/groups/gitlab-org/-/epics/4847)

As the on-demand scans feature is finished, along with the DAST profiles, we will shift our attention to increasing the depth of the DAST scans and the usability of the on-demand scans and profiles. Working to make the dashboard and vulnerability reports more usable, we will begin to aggregate vulnerabilities that are reported on multiple URLs into a single vulnerability. This will greatly reduce the number of vulnerabilities reported by DAST scans and make it much easier to find and fix issues that are found. Looking at increasing the depth of the DAST scans, we will begin to integrate two new scanners into the DAST analyzer. These new scanners will enable DAST to better scan modern single page applications and APIs other than REST, such as GraphQL and SOAP. For the focus on usability of on-demand scans and profiles, we will be working on adding the ability to use the DAST profiles in the CI/CD pipeline scans and creating a scheduler for on-demand scans. 
- [Aggregation of noisy DAST vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/254043)
- [Peach API scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
- [Browserker scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)
- [Scheduled on-demand DAST scans](https://gitlab.com/groups/gitlab-org/-/epics/4876)
- [CI pipeline scan UI configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/276957)
- [DAST profiles in CI pipeline scans](https://gitlab.com/gitlab-org/gitlab/-/issues/216514)

## Maturity Plan
 - [Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2912)
 - [Complete to Lovable](https://gitlab.com/groups/gitlab-org/-/epics/3207)

## Competitive Landscape

- [CA Veracode](https://www.veracode.com/products/dynamic-analysis-dast)
- [Microfocus Fortify WebInspect](https://software.microfocus.com/en-us/products/webinspect-dynamic-analysis-dast/overview)
- [IBM AppScan](https://www.ibm.com/security/application-security/appscan)
- [Rapid7 AppSpider](https://www.rapid7.com/products/appspider)
- [Detectify](https://detectify.com/)

We have an advantage of being able to provide testing results before the app is deployed into the production environment, by using Review Apps. This means that we can provide DAST results for every single commit. The easy integration of DAST early in the software development life cycle is a unique position that GitLab has in the DAST market. Integrating other tools at this stage of the SDLC is typically difficult, at best.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2019 Gartner Quadrant: Application Security Testing](https://www.gartner.com/en/documents/3906990/magic-quadrant-for-application-security-testing)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)

## Top Customer Success/Sales Issue(s)

- [Full list of DAST issues with the "customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=customer&label_name[]=Category%3ADAST)

## Top user issue(s)

- [Full list of DAST issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST)

## Top internal customer issue(s)

- [Full list of DAST issues with the "internal customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST&label_name[]=internal%20customer)

## Top Vision Item(s)

- [On-demand DAST scans](https://gitlab.com/gitlab-org/gitlab/-/issues/214025)
- [Peach API scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
- [Browserker scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)
