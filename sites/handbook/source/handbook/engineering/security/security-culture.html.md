---
layout: handbook-page-toc
title: "Security Culture Committee"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission statement

The security department as a part of GitLab should follow and live up to the GitLab [values](/handbook/values/) and [mission](/company/mission/#mission).
The [transparency](/handbook/values/#transparency) value can be especially difficult for a security department to embrace and embody, as due to the confidentiality of their work, security people tend to be secretive and intransparent by default.

### Intent and goals

The intent of the security culture committee is to maintain a welcoming and transparent environment within the security department.

The committee goals are to:
* Identify areas where our core values can be strengthened
* Improve transparency while maintaining security & privacy across the security organization
* Foster an inviting and welcoming environment for questions, concerns and feedback
* Propose ideas to promote teamwork and collaboration
* Help drive the mission of being the most transparent security group in the world
* Provide actionable feedback and direction to the security department, so they may best live up to the GitLab values

The committee should draft the ways to reach these goals for an open, approachable and transparent culture within the security department. The department’s leadership should reinforce those ways by communicating and leading by example. The committee will provide an interface for all team members to express any concerns regarding the culture within the security department.

## Participation

For the first iteration of this committee the following people were nominated:

* [Dominic Couture](https://gitlab.com/dcouture)
* [Mark Loveless](https://gitlab.com/mloveless)
* [Joern Schneeweisz](https://gitlab.com/joernchen)
* [Heather Simpson](https://gitlab.com/heather)
* [Steve Truong](https://gitlab.com/sttruong)

There will be a six month rotation for the committee members, details on the rotation and follow up nominations will be decided on the first meeting of the committee.

### Meetings

Meetings will take place monthly on the third Thursday 15:00 UTC. The recordings will be available in the "GitLab Videos Recorded" folder.

## Process for change

To suggest a change the [Security Culture](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/new?issuable_template=Security%20Culture) template should be used to put up a suggestion for further discussion among the team if needed.

### Measurement of success

To be determined in the first committee meeting.
