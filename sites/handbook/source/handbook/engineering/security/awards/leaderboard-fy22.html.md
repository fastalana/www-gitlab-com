---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1080 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@manojmj](https://gitlab.com/manojmj) | 3 | 560 |
| [@pks-t](https://gitlab.com/pks-t) | 4 | 500 |
| [@alexpooley](https://gitlab.com/alexpooley) | 5 | 500 |
| [@whaber](https://gitlab.com/whaber) | 6 | 400 |
| [@djadmin](https://gitlab.com/djadmin) | 7 | 400 |
| [@theoretick](https://gitlab.com/theoretick) | 8 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 9 | 400 |
| [@10io](https://gitlab.com/10io) | 10 | 380 |
| [@mrincon](https://gitlab.com/mrincon) | 11 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 12 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 13 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 14 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 15 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 16 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 17 | 300 |
| [@leipert](https://gitlab.com/leipert) | 18 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 19 | 250 |
| [@tkuah](https://gitlab.com/tkuah) | 20 | 240 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 21 | 230 |
| [@mattkasa](https://gitlab.com/mattkasa) | 22 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 23 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 24 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 25 | 200 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 26 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 27 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 28 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 29 | 140 |
| [@kerrizor](https://gitlab.com/kerrizor) | 30 | 130 |
| [@twk3](https://gitlab.com/twk3) | 31 | 130 |
| [@seanarnold](https://gitlab.com/seanarnold) | 32 | 120 |
| [@balasankarc](https://gitlab.com/balasankarc) | 33 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 34 | 100 |
| [@cablett](https://gitlab.com/cablett) | 35 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 36 | 100 |
| [@lauraMon](https://gitlab.com/lauraMon) | 37 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 38 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 39 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 40 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 41 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 42 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 43 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 44 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 45 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 46 | 80 |
| [@splattael](https://gitlab.com/splattael) | 47 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 48 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 49 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 50 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 51 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 52 | 50 |
| [@dgruzd](https://gitlab.com/dgruzd) | 53 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 54 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 55 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 56 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 57 | 40 |
| [@cngo](https://gitlab.com/cngo) | 58 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 59 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 60 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 61 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 62 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 63 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |
| [@tnir](https://gitlab.com/tnir) | 2 | 200 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 6 | 300 |
| [@mrincon](https://gitlab.com/mrincon) | 7 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 8 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 9 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 10 | 200 |
| [@mksionek](https://gitlab.com/mksionek) | 11 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 12 | 120 |
| [@leipert](https://gitlab.com/leipert) | 13 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 14 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 15 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 16 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 17 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 18 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 19 | 60 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 20 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 21 | 40 |
| [@ekigbo](https://gitlab.com/ekigbo) | 22 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 23 | 30 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tnir](https://gitlab.com/tnir) | 1 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 10 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@leipert](https://gitlab.com/leipert) | 13 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 14 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@balasankarc](https://gitlab.com/balasankarc) | 19 | 110 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 20 | 110 |
| [@cablett](https://gitlab.com/cablett) | 21 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 23 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 24 | 100 |
| [@vsizov](https://gitlab.com/vsizov) | 25 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 26 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@splattael](https://gitlab.com/splattael) | 29 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 30 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 31 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 32 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 33 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 34 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@ck3g](https://gitlab.com/ck3g) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@cngo](https://gitlab.com/cngo) | 47 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


