---
layout: handbook-page-toc
title: "Community Advocacy Knowledge base"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This page should serve as a collection of frequently raised topics, and help Advocates re-use good answers from our experts.

## On revenue

- [Are we cash flow positive with our team member number constantly increasing](https://news.ycombinator.com/item?id=18443014)
- I assume that the revenue comes from the enterprise edition?

## On GitLab going public plan

- [GitLab IPO plan](/company/strategy/#sequence)
- [Google](https://news.ycombinator.com/item?id=17748002)

## On stability

- [Pingdom stats link](https://news.ycombinator.com/item?id=21657358)
- [On current performance improvements](https://news.ycombinator.com/item?id=18025846)
- [Post GCP migration metrics](https://www.reddit.com/r/gitlab/comments/9f71nq/thanks_gitlab_team_for_improving_the_stability_of/e673r3t/)

## Emily's response on moving to GitHub

- [Twitter](https://twitter.com/gitlab/status/1082348034858590208)

## On licenses

- [Changing the way we do licensing](https://twitter.com/Jobvo/status/986898325826953217)

## What feature goes in what version?

- [Explained](/company/strategy/#pricing)
- [Short explanation](https://gitlab.com/gitlab-org/gitlab-ee/#editions)
- [Feature Comparison](/pricing/saas/feature-comparison/)
- [Upcoming releases](/upcoming-releases/)
- [Paid-only features](/company/stewardship/#what-features-are-paid-only)

## On the database incident

## GitLab vs GitHub?

- [Address the awareness-gap](https://news.ycombinator.com/item?id=18026118)

## On paying local rates

- [Handbook](/handbook/total-rewards/compensation/#paying-local-rates)
- [Relocating](https://news.ycombinator.com/item?id=18028119)

## Bad experiences with CI/CD

- [CI/AutoDevOps vision](https://news.ycombinator.com/item?id=18026019)

## On Serverless
- [Handbook FAQ](/handbook/marketing/strategic-marketing/enablement/serverless-faq/)
- [Landing page](/stages-devops-lifecycle/serverless/)
- [Product vision](https://gitlab.com/groups/gitlab-org/-/epics/155)
- [Community feedback on HackerNews](https://news.ycombinator.com/item?id=18661061)
- [Example of Sid's overview/comment on HackerNews](https://news.ycombinator.com/item?id=18689820)

## Testimonials
- [HN user: GitLab > GitHub](https://news.ycombinator.com/item?id=18854487)
- [Migration to GitLab story](https://twitter.com/monktoninc/status/1083346318741901312)
