---
layout: handbook-page-toc
title: "Channel Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## This page is intended for the following audiences

GitLab global and regional channel sales teams, global and local field sales teams, regional [field marketing](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) teams, channel account managers, the global alliances team and product marketing team.

## Our Team
Global Channel Marketing is a team of seasoned professionals supporting global channel sales objectives and providing support to the GitLab channel partner community.  This small but mighty team is responsible for the development of channel marketing campaigns that leverage GitLab’s FY22 Go to Market Campaigns and sales plays.  We work with partners to help them understand what the campaigns are, how to use them to help drive partner sourced opportunities, how to leverage upcoming GitLab webinars/webcasts to drive conversion and how to generate trials for GitLab that they can nurture with prospects.  Channel partners are ultimately responsible for the execution of these campaigns.  

The Channel marketing team provides support to partners by creating different campaign assets into leverageable go-to-market programs called Campaigns-in-a-box that partners can easily pick up and run with their customer or prospect lists.  GitLab channel marketing is also responsible for the development, rollout and management of trial enablement programs by which partners can generate trials of GitLab directly from their website, and passing or providing trial leads generated from GitLab’s own website to be worked and converted by partners.  

Channel marketing also enables channel go to market efforts through MDF funding and management, by identifying upcoming field marketing events and activities that channel partners can participate in, and by identifying opportunities for channel partners to participate with Alliances partners and GitLab on joint go to market activities.

We partner with regional Channel Account Managers and Field Marketing teams to work closely with channel partners and GitLab Alliances for inclusion in their field marketing plans.  

As a service bureau to a wide variety of teams, we have support functions that are both in and out of scope at this time.  We have ideated on a list of potential future service capabilities that, as this team is able to add resources and our business plan requires it, we will add to our list of service offerings.  To recommend that we add a service offering to this growing list of potential offerings, please create an issue.

## Current Service Offerings as of Q1 FY22

*   Administering Marketing Development Fund (MDF) to provide financial reimbursement for APPROVED partner marketing campaigns and events.  Requests must support GitLab GTM initiatives and adhere to the MDF Request Process detailed below. MDF reimbursement will be at 50% of the total campaign request.  There are three approval levels and each level has 2 business days to review and approve/decline the MDF proposal. 
*   Turnkey, integrated demand generation campaigns-in-a-box global and where appropriate, regional
    *   Support and funding for translation and localization of campaign assets from English to another language for [priority countries](https://docs.google.com/spreadsheets/d/1eRrtRPdNSQjtvDrEvPJ_klfqKAatnLIzDjvShXhnSr8/edit?usp=sharing) will be considered upon request For additional information on localization, please visit the [Localization](https://about.gitlab.com/handbook/marketing/localization/#priority-countries) Handbook page.
*   Global [external virtual events, workshops](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/), and third party events and webcasts  leveraging Alliance partner “better together” messaging and joint GTM solution customer value propositions.
*   In partnership with [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/): 
    *   Develop marketing campaign that targets partner Solution Architects to become advocates of GitLab by delivering GitLab solution overviews, demos and proof of concepts to uncover and convert sales opportunities
    *   Enable channel partner marketers to participate in [FY22 global GTM](https://about.gitlab.com/handbook/marketing/plan-fy22/) motions in partnership with the [GTM team](https://about.gitlab.com/handbook/marketing/plan-fy22/#core-teams).  
        *   Build and deliver a Bill of Materials of channel-friendly and consumable versions of GitLab campaigns and assets 
*   Provide review and approval for local/regional [field marketing](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) plans with partners
*   Provide review and approval of Channel Account Manager (CAM) marketing plans.
*   Build a repeatable, predictable, sustainable and desirable model enabling partners to receive, accept, nurture and close GitLab leads leveraging the GitLab Partner Portal and Vartopia Deal Registration system.  (future link to documentation of the process)
*   Enable channel partners to add “[GitLab free trial](https://about.gitlab.com/free-trial/self-managed/)” functionality to their websites with marketing tools and lead passing to allow partners to effectively generate and nurture their own leads.  This will serve as partner primary CTA within demand generation efforts. 
*   For Select Partners, participate in partner QBRs and provide input and ideation on successful go to market campaigns and initiatives to help partners drive more business with GitLab. 
*   On a monthly basis, provide marketing content and input to the Global Channel Operations team for use in the global channel communication/newsletter 
*   On a quarterly basis, host global marketing webcasts to the partner community.
*   Support GitLab [Analyst Relations](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/) requests for channel program input on MQs, WAVEs, market guides, as needed and with third party channel analysts.  
    *   Support requests for inclusion in third party publications
*   Support the web team in maintaining GitLab’s marketing website [partner pages](https://about.gitlab.com/partners/)
*   Working with Sales Ops and Marketing Ops improve attribution and reporting of [partner sourced leads (CQLs), and partner sourced opportunities](t.gitlab.com/handbook/sales/field-operations/channel-operations/). 

## Quarterly Planning: Field and Channel

As GitLab evolves, the growth of the channel becomes a critical component to our overall strategy of growth and new logo capture.  With established teams in place, it's time to take our engagement up a level and as such, review how we partner together for proactive, and efficient engagement that results in high quality demand generation plans and delivering actionable leads for our sales teams.
In order for our collective plans to work, it's first important to verbalize the roles, responsibilities and focus areas for each functional team.
*   ## Channel Account Managers 
Own the strategy, relationship and ongoing, consistent engagement with the Select partners in their territory.  The channel account manager is responsible for sharing a gap analysis of their business to the respective marketing teams to develop a plan for support.  This should occur no less than 6 weeks prior to the start of Q3.
*   ## GEO Channel Director
 Sets the strategy for the region, manages the Channel Account Managers and approves the criteria for Select and Open partner selection in the region.
*   ## Field Marketing Managers
In partnership with field sales, drive MQL's to convert to SAO's.  The field marketing managers are responsible for articulating which areas of the business require the most attention and where we need channel partner engagement to drive demand generation plans.
*   ## GEO Field Marketing Director
Develops the strategy and conversion targets for the team in partnership with field sales leadership.
*   ## Global Channel Marketing Director
Develops scalable demand generation campaigns and tactics for partners to generate Partner Sourced opportunities.

Once a plan for the quarter has been established it is imperative that the channel account manager and the field marketing manager stay in close, consistent alignment about plan execution.  The strategy is driven by the channel account manager, while the field marketing manager is responsible for executing on that strategy in partnership with various support teams, including but not limited to the channel account managers.

[MDF](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds) 
CAN and SHOULD be leveraged as much as possible when the partner is involved in an activity.  Further information about how to initiate and MDF request can be found here.

## Tracking Success:
GitLab Marketing receives 'credit' for opportunities that are partner sourced so long as the oportunity has a BATP (Bizable Touchpoint) from the campaign.
Check out the [field marketing handbook page](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-initiated-and-funded-campaigns)
that details the FMM process where CAMs can read more about how to complete [the lead gen issue.](https://gitlab.com/gitlab-com/marketing/field-marketing/-.issues/new?issuable_template=Channel_LeadGen_Req)

## Requesting MDF funds

- Approved Select Partners will submit [MDF proposals through the partner portal](https://docs.google.com/forms/d/1DTpKzLHiQmai7clipbHCv0pHX9Qz9EymnMHGPMMz3To/viewform?edit_requested=true), and GitLab approvers will be notified of the request via email generated from the Google form.
    + The Channel Marketing DRI will create a confidential new issue [`new_channel_mdf_request`](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#new_channel_mdf_proposal) for the review and approval process for the MDF proposal - This kicks off the official process
       +  All documentation, analysis, thoughts and approvals will be captured within that issue
       +  Each issue will be associated to the appropriate `Quarterly MDF Epic`
    +  The MDF proposal will go through 3 levels of approvals 
       + Level 1 approval - Channel Account Manager (CAM)
       + Level 2 approval - Channel Marketing
       + Level 3 approval - Field Marketing
          + For efficiency purposes, Channel Marketing will assign the Level 3 approver for FMM to the appropriate FMM Manager for the region and PubSec
          + The FMM Manager will then allocate to the appropriate FMM manager in the region of the MDF request. (see issue template for more details) 
    +  Once the MDF request has been either approved or declined the partner will be notified: 
          + If declined, the Channel Marketing DRI will reach out to the partner and let them know. We will copy the CAM on the notificiation
          + If approved, the FMM will be the DRI for reaching out to the partner contact in the proposal to review dates of execution and provide any other guidance

### Submitting Proof of Performance (POP) for MDF 

- Once the MDF campaign is completed, the partner has 30 days to submit their [Proof of Performance (POP)](https://docs.google.com/forms/d/e/1FAIpQLSeb_r6c7xo4KdxXWbwelmAJiDyVAF5UxMxeppypH2C0oQ8vhQ/viewform) for reimbursement.
- Once a POP is completed, the Channel Marketing Manager will create an issue [`channel_mdf_pop`](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#channel_mdf_pop) and complete Step 1 
- Once Step 1 is completed, Channel Marketing will notify Finance for their review and reimbursement payment to the partner

## Future/Potential Service Offerings



*   Branding, awareness and promotion of GitLab channel partner badges
*   Partner marketing concierge services (dependent on funding/resources)
*   Joint demand generation campaigns with partners
*   Partner Sales Engineer/Solution Architect marketing program (to become advocates of GitLab)


## Beyond our Team’s Scope



*   Individual partner support, planning, ideation and execution including but not limited to joint demand generation campaigns.  Please work closely with your region’s [field marketing](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) team and reference linked Handbook pages for self support and field support of these programs.
*   Custom campaigns: Channel Marketing does not currently have the resources to support individual partner campaigns and events.  Please encourage your partners to leverage one of our turnkey, and integrated campaigns-in-a-box.  For unique virtual and live events, the CAM should engage with their [field marketing](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) counterpart to leverage field budget and marketing support for joint events.  The Field Marketing Manager will engage with their sales teams and support the CAM and partner with development, execution, and lead follow up for  the event.
*   Asset creation: Channel Marketing is unable to work directly with partners to customize their marketing assets. 
*   Joint demand generation planning and execution:** **The GitLab CAM is the primary point of contact to enable their partners run a successful campaign or event.  
*   Event speakers: The Channel Marketing team does not have the resources to help locate GitLab speakers for partner events.
*   Partner Blogs: The Channel Marketing team does not have the resources to craft unique content to support a partner blog or content request.
*   Press releases: The Channel Marketing team does not have the responsibility to edit and approve partner press releases.  Please create a [Channel or Tech Partner Announcement issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=channel-partner-announcement-request) for coordination with the Corporate Communications/PR team
*   Sponsored social media posts: partners looking to GitLab to promote partner activities would not route those requests through Channel Marketing.  Instead, CAM’s obtain the partner’s social media channels and [create an issue](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#open-a-new-issue-to-request-social-coverage) for the Social Media team.  This request should include the partner’s social media information, detailed description of what we are being asked to promote and target dates.  
*   Event attendance: The Channel Marketing team does not own a database of contacts by which to drive attendance to partner specific events nor do we possess the resources to support such requests.
*   Partner training:**<span style="text-decoration:underline;"> [Partner training](https://about.gitlab.com/handbook/resellers/training/)</span>** is managed and supported by 
*   Partner portal management: [Partner portal administration](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/#partner-portal-administration) is managed by GitLab Channel Program Operations.

## Service Level Agreement

When requesting support from our team, we commit to responding to your [issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new?issuable_template=channel_partner_request) within 2 business days (when request is easily understood) or a Zoom meeting invite to understand the request, scope, timeline, goals and expectations.  

## Meet the Team



*   **David Duncan**: (aka Dunk) VP of Demand Generation & Partner Marketing
*   **Coleen Greco**: (aka Greco) Director, Global Channel and Alliances Marketing
*   **Lisa Rom**: Senior Partner Marketing Manager.  Lisa will be focusing on global alliances and channel marketing initiatives and major channel programs
*   **Karen Pourshadi**: Senior Partner Marketing Manager.  Karen will be focused on content: enabling our partner marketing resources with major GitLab GTM initiatives and developing turnkey, integrated campaigns in a box for GitLab channel partners. 
*   **Mon Ray**: Senior Partner Marketing Manager.  Mon will be focused on developing core Alliances initiatives and content.

## The best way to contact our team is through our Slack channels

#channel-marketing 

#alliances_partnermktg
