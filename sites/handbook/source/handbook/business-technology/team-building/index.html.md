---
layout: handbook-page-toc
title: Business Technology Team Building
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Team Building at Business Technology

## CIO Speaker Series

- [Unfiltered Playlist](https://www.youtube.com/watch?v=dHtIR7Oya6o&list=PL05JrBw4t0Kom2dWuFd-KoVnZqUTmlZQJ)
    - **Shobhana (Shobz) Ahluwalia, CIO, Peloton Interactive** - [2021-05-5. **Topic:** Diversity and Inclusion](https://www.youtube.com/watch?v=frHW0fNbm5c&list=PL05JrBw4t0Kom2dWuFd-KoVnZqUTmlZQJ&index=6)
    - **Mark Settle, 7xCIO, Author** - [2020-10-14. **Topic:** Discuss Truth from the Valley: A practical Primer on IT Management for the Next Decade](https://www.youtube.com/watch?v=MLAboHCE434)
    - **Eric Johnson, CIO, Survey Monkey** - [2020-09-16. **Topic:** Business Engagement](https://www.youtube.com/watch?v=dHtIR7Oya6o)
    - **Stephen Franchetti, CIO and VP of Business Technology, Slack** - [2020-08-19. **Topic:** Business Technology Rebranding](https://youtu.be/j9vjNMVuL9c)

## Book Club

Book Club: [Truth from the Valley: A Practical Primer on Future IT Management Trends](https://gitlab.com/gitlab-com/book-clubs/-/issues/12)


## Technology Days

Let's discover new technologies through conversation, collaboration, demonstration, and instruction based on professional experiences and current projects being worked on within our department. As a participant you will have an inside track on thought-leadership, trends, and practical experience. During our cross-team social and technical sessions running once a month, we will get to understand the challenges other teams experience and collaborate on solving them.

**2021-07-16:** 
Nest.js Framework. 
    **Lead by:** Integrations team Daniel Parker and Karuna Singh.


**2021-08-20:** GitLab Product. 
    **Lead by:** Lis Vinueza.


## Monthly Social Calls

During the last Thursday and Friday of each month, we host 2 different social hours - APAC-AMER and APAC-EMEA. During the social hours we play one of the following games:

- [Gartic Phone](https://garticphone.com/)
- [Among us](https://innersloth.com/gameAmongUs.php) 
- [Pinturillo](https://www.pinturillo2.com/) 


## Quarterly Team Building 

GitLab provides a [quarterly budget for team building](https://about.gitlab.com/handbook/finance/expenses/#team-building-budget). 

- FY21 Q2 - Finance Division Escape Room
- FY22 Q1 - Business Technology Pizza Party
- FY22 Q2 - TBD
