---
layout: handbook-page-toc
title: "Sales Guide: Collaborating with GitLab Legal"
description: "This resource provides assistance to the GitLab Sales Team on operational and educational elements of working with GitLab Legal for Customer negotiations"
---

## OVERVIEW 
### 🎥 [Watch Overview Video](https://www.youtube.com/watch?time_continue=2&v=snb-1ENQitI&feature=emb_logo) 
*Learn about the purpose of this resource; 1:40 (Log into GitLab Unfiltered)*

Thank you for visiting! The purpose of this resource is to provide Sales reps assistance on: 
    
(1) [Operational](https://about.gitlab.com/handbook/legal/customer-negotiations/#operational) - How to work with, and engage, GitLab legal; Requirements to close a deal; General FAQ for legal ops, including some parallels to Finance and Deal Desk matters. 

(2) [Educational](https://about.gitlab.com/handbook/legal/customer-negotiations/#educational) - Learning about how and why GitLab legal and our agreements work, to better serve you during a sales cycle; Proactive advice and education to enable you to feel more confident when / if your Customer has legal related questions.   


## ENGAGEMENT
- As part of this collaboration, we are measuring success by interaction with the field. For this reason, we ask that you **please watch the videos and "thumbs-up" or "like" them.**
- **Please Note**: All videos provided will be up on the GitLab Unfiltered YouTube Account. You will need to login to this Account in order to view. 


## REQUESTING CONTENT
- You are always encouraged to make requests for future content that will help you and the team. Simply complete the form [here](https://forms.gle/2zznmLFznSqJAQUH6).


| OPERATIONAL| EDUCATIONAL |
| ------ | ------ |
| [**How to reach Legal** ](#how-to-reach-legal)                  | [**Overview of GitLab Agreements**](#overview-of-gitlab-agreements)  |
| [**How do I get an Agreement signed**](#how-do-i-get-an-agreement-signed)       | [**When does GitLab negotiate**](#when-does-gitlab-negotiate)  |
| [**Clearing Export Review in SFDC** ](#clear-export-review-in-sfdc)     | [**SKO 2021 Resources**](#gitlab-legal-sko-2021-resources) |
|[**Completing Vendor Request Forms** ](#completing-vendor-request-forms) | [**What is Open Source Software?**](#open-source) |
| [**What's needed to close a deal?**](#whats-needed-to-close-a-deal)    | [**GitLab Subscription Agreement 101**](#gitlab-subscription-agreement-101) |
| [**Requests for GitLab Financial Information, Tax and Insurance Certificates**](#completing-vendor-request-forms) | [**Why GitLab does not sign BAAs (Business Associate Agreement)**](#why-gitlab-does-not-sign-baas) |
| [**How to get a Data Processing Agreement (DPA) Signed**](#how-to-get-a-data-processing-addendum-dpa-signed) | [**Intro to GitLab Privacy Compliance and Data Processing Agreement (DPA)**](#intro-to-data-privacy-and-the-gitlab-data-processing-addendum) |

## OPERATIONAL

## How to reach Legal 
### 🎥 [Watch the "How to Reach Legal" Video](https://www.youtube.com/embed/bCLagn_sWSI)
*Learn about how to open a Legal Request, When to Open a Legal Request; 2:24 (Log into GitLab Unfiltered)*

1. For all questions related to Partners and/or Customers, including: NDA's, Contract Review / Negotiations, Legal Questions...etc., please open a **Legal Request** in SFDC. 
2. Please note: There is a $25K ARR opportunity minimum threshold to review edits to our terms, and a $100K ARR opportunity minimum threshold to review Customer / Partner Terms. These thresholds do not apply to NDA's. 
3. Step-by-Step directions on opening a Legal Request can be found [here](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#request-editable-version-of-gitlab-template)
_NOTE:_ Once you open a Legal Request for "Contract Review", **DO NOT** open another Legal Request when updated red-lines are provided. All red-lines / negotiated versions will be stored in one (1) Legal Request to ensure version history is maintained. 
4. A presentation on this topic can be found [here](https://docs.google.com/presentation/d/1lesWNvPAFd1B3RuCgKsqQlE85ZEwLuE01QpVAKPhQKw/edit#slide=id.g5d6196cc9d_2_0)

**Additional Resources:**
[Field Operations Legal Request Overview](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#contact-legal) 

## How do I get an Agreement signed
*Learn about how to get an Agreement Signed; Who can sign on behalf of GitLab*

1. **Sales Reps cannot sign any Agreements.** Only certain individuals may execute on behalf of GitLab see Authorization Matrix [here](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix). 
2. All Agreements require a GitLab Legal stamp in order to be executed. This stamp is placed by a Contract Manager when an executable version is reached. 
3. Once the Agreement has been stamped by GitLab legal, follow the [DocuSign Process](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-send-an-order-form-to-the-customer-for-signature-via-docusign) to send the Agreement for signature within SFDC. 

**Additional Resources:**
Visit the [Sales Order Processing](https://about.gitlab.com/handbook/sales/field-operations/order-processing/) for helpful information

## Clear Export Review in SFDC
1. See the [Visual Compliance Overview](https://docs.google.com/presentation/d/1hD1xgEyJL1U1eyvTci6NNlCdIur4PkB16GO52wjZRgk/edit#slide=id.g5dbc2f529a_0_41)
2. GitLab uses Visual Compliance to review all Accounts. The tool is continually reviewing and 'clearing' Accounts. In addition, any 'flags' / 'hits' are reviewed and cleared throughout the day by GitLab Legal.
3. If an export notice is presented, that requires immediate action, please tag "@legal" in SFDC Chatter for the Account and write "Please review for export compliance"
4. Legal will continually review all requests. Once cleared in Visual Compliance update(s) will reaching SFDC in 15-20 minutes. 

## Completing Vendor Request Forms

**It is the sales team member responsibility to complete Vendor Request Forms**
1. Much, if not all, of the Vendor Form information can be found at the [Company Information Page](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information)

**Included on the Company Information Page is:**
- Address / information related to each GitLab entity
- All banking information
- ECCN Number
- NAICS Code
- Dun and Bradstreet Number
...AND VARIOUS OTHER DETAILS
- W9 is located on the Finance [Forms page](https://about.gitlab.com/handbook/finance/#forms)
- Request Insurance Certificate on [Legal Team Page](https://about.gitlab.com/handbook/legal/#4-requests-for-insurance-certificate)

2.  If there are questions / elements that are not found in the Company Information Page, the Sales Rep should engage Deal Desk to organize requests of finance and tax. Information regarding contacting Deal Desk and overall process can be found at the [Sales Order Process Page](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-process-customer-requested-vendor-setup-forms)

3. **ONLY OPEN A LEGAL REQUEST IF:** There is a specific legal question / element being requested within the Vendor Form.

Example: Has GitLab, or board member / executed of GitLab, been subject to a lawsuit? 
 
**As a private company, we will not share the following information in a Vendor Request Form:** 
- Stock / Share ownership of the company
- Finances, including annual and/or historical performance 

## What's needed to close a Deal
### 🎥 Watch the ["What's needed to close a Deal" Video](https://www.youtube.com/watch?v=EhxHY6xk938)

1. Visit the [Sales Operations](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/) and [Deal Desk](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/) Handbook materials to review how to create an Order Form. 
2. An Order Form with a base Agreement referenced. This will either be the current [Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/) as found at the [Terms of Service](https://about.gitlab.com/terms/#current-terms-of-use), or, if an executed Agreement is signed, this will be referenced within the Order Form. 
3. Visit the presentation on the [Legal Requirements](https://docs.google.com/presentation/d/1IaGO8j--WOKZqAwuBQjKJJN0hU7rfpdgbIoj_kClGK0/edit#slide=id.g5d6196cc9d_2_219) _Note: This is locked for GitLab Team Members only_
4. The fastest way to close a deal is to ensure the Customer signs the Order Form. If a Customer refuses to sign an Order Form, additional approvals will be required by both GitLab Legal and Finance team members. In addition, the Customer may need to make changes to their Purchase Order (PO). 


## Requests for GitLab Financial Information, Tax and Insurance Certificates

1. For Tax Certificate requests please email the Finance team at [tax@gitlab.com](mailto:tax@gitlab.com)
2. For Insurance Certificate requests please work with the Deal Desk team by tagging @sales-support in SFDC Chatter.  
3. Regarding GitLab financial information (e.g., annual statements, equity ownership...etc) please note that GitLab is a private company and does not share this information with Customers.  


## How to get a Data Processing Addendum (DPA) signed
1. GitLab includes a signed version of the DPA on the [Terms of Service](https://about.gitlab.com/terms/), as stated within the [Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/) this DPA is applicable to all Enterprise Customer's and does not need to be counter-signed. In addition, as stated within the header of the DPA, a Customer’s acceptance of the Subscription Agreement shall be treated as its execution of the DPA.  


## EDUCATIONAL

## Overview of GitLab Agreements
### 🎥 [Watch the "Overview of GitLab Agreements" Video](https://www.youtube.com/embed/M3ktHGDBebw)
*Learn about what Agreements GitLab has; 2:45 (Log into GitLab Unfiltered)*

1. GitLab provides our Software pursuant to the GitLab Subscription Agreement, and Professional Services pursuant to GitLab Professional Services Terms;
2. GitLab has a Master Partner Agreement that includes multiple Exhibits to enable Partners to, (i) Reseller, (ii) Refer, and (iii) Distribute GitLab Products and Services;
3. Other than web-portal purchases, GitLab enters into an Order Form with a Customer which outlines the Products and Services being sold;
4. Any modification(s) to Order Forms, or standard Terms and Conditions, require GitLab Legal Approval. 

## When does GitLab negotiate
### 🎥 [Watch the "When does GitLab Negotiate Video"](https://www.youtube.com/embed/zYvZKP5OohA)
*Learn about when GitLab negotiates; Where to find an NDA template; 3:20 (Log into GitLab Unfiltered)*

1. GitLab will negotiate NDAs with our prospects. You can always send the prospect the GitLab NDA template and ask for them to sign. If they want to use their template, simply open a Legal Request for "Contract Review".
2. GitLab will negotiate changes to our terms and conditions provided the [current] Opportunity is greater than $25K (USD).
3. GitLab will negotiate using a Customer terms and conditions-if they are applicable to our Software and business-if the [current] Opportunity is greater than $100K (USD).
4. **Why don't we negotiate with all Customers?** Using resources to negotiate on all deals would not be a good use of GitLab resources and instead, we created our terms to be reasonable and in line with industry standards which allows us to place our resources into creating better products and services. 

## GitLab Legal SKO 2021 Resources
### [Look through the GitLab Legal "Legalize IT!" Presentation](https://docs.google.com/presentation/d/11HK5GD4gyE2j2z0swLe644tQWmc3XFyvtM4tytYpIzI/edit?usp=sharing) 


1. Learn about the GitLab Legal Team! Look through basic questions and answers which will help you collaborate with legal and close deals faster!


## Open Source

1. For a general introduction to Open Source, including lists of acceptable and unacceptable Open Source licenses for use in GitLab, see [Open Source at GitLab](https://about.gitlab.com/handbook/engineering/open-source/).
1. Helpful external resources on Open Source Software can be found at [Opensource.com](https://opensource.com/) and the [Open Source Inititiave](https://opensource.org/).
3. The Open Source project for GitLab can be found at [gitlab.com/gitlab-org/gitlab](gitlab.com/gitlab-org/gitlab).
4. GitLab uses Open Source Software as part of its Open Core Business Model. Learn more about the [GitLab Open Core Business Model](https://about.gitlab.com/company/stewardship/#business-model).


## GitLab Subscription Agreement 101
### 🎥 Watch the ["Subscription Agreement Basics" Video](https://www.youtube.com/watch?v=jaX81XXD55w) 
_Please Note: The purpose of this content is to understand where to find GitLab's terms, as well as, basic knowlege about the rights and obligations stated_

1. GitLab terms can be found at our [Terms of Service](https://about.gitlab.com/terms/)
2. GitLab provides full transparency by including historic versions of the Terms. This can be found within the [Agreement History](https://about.gitlab.com/terms/#agreement-history) section
3. The [Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/) can be used for the sale of both On-Premise and SaaS Software 
4. The Subscription Agreement is agreed to by either, (i) Clicking-through when purchasing (or downloading) via the GitLab Website, (ii) Referenced in an Order Form that is signed by a Customer, or (iii) Passed through a Reseller if a Customer is buying through an Authorized Partner 

 
## Why GitLab does not sign BAAs

1. **GitLab will not sign a Business Associate Agreement (BAA)**

2. Resources to provide your Customer if they request a BAA:

**What do the GitLab Terms say?**
- GitLab is transparent with respect to information it requires, or does not require, in order to provide our products and services. Please view [Section 14.3 of the Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/#14-security--data-protection), which specifically highlights that GitLab will not receive Personal Heatlh Information. 

**Why does GitLab not sign BAAs?**
- GitLab does not require nor is designed to be used to store Personal Health Information (PHI). In addition, GitLab, as an organiztion, does not desire or need to receive, process, store, transact or otherwise possess PHI in order to provide our products and servcies. As such, GitLab does not meet, or desire to meet, the definition of a "Business Associate" as defined under the Health Insurance Portability and Accountability Act (HIPAA).  
- In the event a Customer questions "incidental dislcosures", please highlight the HIPAA definition itself, which states: _“Business associate functions or activities on behalf of a covered entity include claims processing, data analysis, utilization review, and billing.  Business associate services to a covered entity are limited to legal, actuarial, accounting, consulting, data aggregation, management, administrative, accreditation, or financial services. **However, persons or organizations are not considered business associates if their functions or services do not involve the use or disclosure of protected health information, and where any access to protected health information by such persons would be incidental, if at all.**”_

## Intro to Data Privacy and the GitLab Data Processing Addendum

1. The [GitLab Privacy Policy](https://about.gitlab.com/privacy/) explains how GitLab collects, uses and shares customers' personal information, and how they may exercise their rights in respect of that information. More information about privacy compliance at GitLab, including answers to a number of frequently asked questions concerning the GDPR, can be found on the [GitLab Privacy Compliance](https://about.gitlab.com/privacy/privacy-compliance/) handbook page.
1. The GitLab Data Processing Addendum, usually referred to as the "DPA", can be accessed from the [GitLab Terms of Service page](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/BR_signed_Hosted_DPA_V1-brobins_gitlab.com.pdf). As stated in the [GitLab Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/#14-security--data-protection), the terms of the DPA automatically apply to corporate customers.


## Additional Resources
[Field Operations Negotiation Overview](/handbook/sales/field-operations/order-processing/#using-customer-form-agreements-and-negotiating-gitlabs-standard-agreement)


