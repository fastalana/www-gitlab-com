---
layout: handbook-page-toc
title: Work with the Learning and Development Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Available Learning and Development Support

The Learning and Development team is available to support GitLab teams in the following capacities. Requests will be evaluated as they come in based on team's capacity for support, business impact, correlation to company OKRs, and number of impacted users.

| Support Model | When to use this process |
| ----- | ----- |
| Hosting a Live Learning event | You'd like to **host** an internal Live Learning session |
| Creating a GitLab Learn Channel | You'd like to build a new Channel in GitLab Learn to curate professional development content for your team |
| Creating a new learning pathway | Create a new learning pathway or course for any audience |
| Uploading to GitLab Learn | Add new or existing learning content to the GitLab Learn LXP |
| Integrating external content to GitLab Learn | Add off-the-shelf learning from another provider to GitLab Learn |
| Requesting content review | L&D review of your learning content for handbook first, adult learning theory, and bias for async alignment |
| Requesting new content creation | You'd like the L&D team to **create** or **host** learning content for your team |


### Host a Live Learning event

Anyone at GitLab should feel empowered to host a learning session. Why would you want to host a live learning event? Maybe **you** are the expert on something. Or maybe you just want to learn more about a specific topic and share those learnings with more people. 

To host a live learning event, start by opening an issue in the Learning and Development [general project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues) using the `live_learning` template. All steps should be outlined on this template.  

**Examples of past Live Learning events**: 
- [Introduction to Coaching](/handbook/leadership/coaching/#introduction-to-coaching-1)
- [Being an Ally](/company/culture/inclusion/being-an-ally/#ally-training)

### Creating a Learning Hub for your team

Check out this discussion between Product and L&D understand how your team can use GitLab Learn to create a Learning Hub for your team:

<iframe width="560" height="315" src="https://www.youtube.com/embed/xERAnmwW0G0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

We work closely with leaders across GitLab to develop landing pages for learning material that is tailored to specific departments and teams. By creating [Channels in GitLab Learn](/handbook/people-group/learning-and-development/gitlab-learn/admin/#broadcasting-on-channels), teams can contribute, curate, organize, and suggest professional development opportunities for their team members.  Some scenarios where you might want to build a Channel include:

1. You have multiple LinkedIn Learning courses you'd like to showcase
1. You'd like to track what courses your team is completing
1. You'd like a single spot to send team members who are looking for professional development courses

Here are some examples of Channels that teams have created in GitLab Learn to serve as a Learning Hub for their department or team:

1. [Product Manager Learning Hub](https://gitlab.edcast.com/channel/gitlab-product-team-learning-hub)
1. [Support Team Learning Hub](https://gitlab.edcast.com/channel/gitlab-support-team-professional-development)
1. [Marketing Team Learning Hub](https://gitlab.edcast.com/channel/gitlab-marketing-learning-hub)
1. [Finance Team Learning Hub](https://gitlab.edcast.com/channel/gitlab-finance-learning-hub)
1. [Solution Architect Learning Hub](https://gitlab.edcast.com/channel/gitlab-solution-architect-learning-hub)

To create a Learning Hub for your team using a Channel in GitLab Learn, complete these two steps:

1. Open an issue in the Learning and Development [general project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues) using the `new_channel` [template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/new?issuable_template=new_channel). All steps should be outlined on this template. 
1. Take the [Learning Curator training](https://gitlab.edcast.com/pathways/learning-curator-training) in GitLab Learn 

**In future iterations, the L&D team will enable team members and managers to curate and promote content on these channels without needing to involve the L&D team.**

### Create a new learning pathway

Anyone team at GitLab is encoruaged to build new learning pathways. Some scenarios where you might want to build a pathway include:

1. Create career development path for your team
1. Build a training for external audiences on your role
1. Teach the GitLab team how to efficiently solve a common problem, like troubles with Merge Requests or strategies for async communication
1. Organize content you've found externally, like articles, videos, or podcasts

To create a new learning pathway and to add that pathway to GitLab Learn, start by opening an issue in the Learning and Development [general project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues) using the `new_pathway` [template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/new#). All steps should be outlined on this template.  

**Examples of learning paths in GitLab Learn

1. Diversity, Inclusion and Belonging Certification
1. GitLab 101 and GitLab 201
1. Bias for Asynchronous Communication

**Timeline and expectations**

|Action | Timeline |
| ----- | ----- |
| Build course content in the handbook | this is up to your team, but please build the content before opening a `new_pathway` issue |
| Open a `new_pathway` [issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/new#) | 5 weeks prior to launch |
| Start collaboration on pathway needs with L&D team | 4 week prior to launch |
| Implement changes on handbook based on L&D feedback | 3 weeks prior to launch |
| Complete Learning Evangelist training in GitLab Learn | 3 weeks prior to launch |
| First iteration of conent upload to GitLab Learn | 2 weeks prior to launch |
| Peer review or L&D review of content in GitLab Learn | 1 week prior to launch |
| Make final edits based on peer review | 1 week prior to launch |
| Launch your course | approx. **5 weeks** after opening the `new_pathway` issue with the L&D team |


### Integrate external content to GitLab Learn

Teams might be interested in integrating external training platforms and resources into GitLab Learn. Integrating these trainings into GitLab Learn can be a great way to keep learning resources for team members in one place and to track completions.

To start looking into the potential of integrating an external learning source into GitLab Learn, start by opening an issue in the Learning and Development [general project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues) using the `external_learning_content` template. All steps should be outlined on this template.  


### Request content review

The process for L&D content review is being built.

**Timeline and expectations**

Timeline and expectations for this type of request are currently a WIP.



### Request new content creation or L&D support

The process for requesting content from L&D is being built.

**Timeline and expectations**

Timeline and expectations for this type of request are currently a WIP.


