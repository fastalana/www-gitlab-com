---
layout: job_family_page
title: "Total Rewards"
---

The Total Rewards job family is responsible for all the tools available so that GitLab may be attract, motivate and retain team members. [Total Rewards](/handbook/total-rewards/) encompasses the elements of compensation, well-being, benefits, and recognition that, when done correctly, lead to optimal organizational performance. Total Rewards is designed strategically and executed in alignment with business goals, Total Rewards programs fuel motivated and productive team members that are appreciated and rewarded for their contributions, driving GitLab to ever greater success.

## Total Rewards Coordinator

### Job Grade
The Total Rewards Coordinator is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- HRIS data entry.
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date
  - Coordinate any changes with payroll
- Collaborate with the People group on Total Rewards policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Process and submit payment for all entity benefits billing (US residency is a requirement to process US benefits) in collaboration with Accounts Payable. 
- Audit all benefits invoices to ensure accuracy. 
- Complete Access Requests for HRIS data access. 
- Update Total Rewards documentation as directed.
- Keep it efficient and DRY.

### Requirements
- 1-2 years experience in an HR or People Operations role, concentration on total rewards preferred
- For US Billing, required to be a US resident
- Bachelor's degree in Mathematics, Business, or HR preferred
- Excellent written and verbal communication skills
- Enthusiasm for software tools
- Proven experience quickly learning new software tools
- Willing to make People Operations as open and transparent as possible
- Desire to work for a fast moving startup
- You share our [values](/handbook/values/), and work in accordance with those values
- Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks).
- Ability to use GitLab

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)

## Total Rewards Analyst

### Job Grade
The Total Rewards Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Coordinate on compensation strategies and principles.
- Participate in compensation and benefits surveys as directed.
- HRIS data entry.
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date
  - Coordinate any changes with payroll
- Collaborate with the People Ops team on People Operations policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Coordinate on benefits administration.
  - Audit and pay all US benefit invoices.
- Collect data to track trends in functional areas.
- Assist with training employees on various topics.
- Maintain approval requests in Greenhouse ensuring a job family and compensation benchmark are set.
- Keep it efficient and DRY.

### Requirements
- 2-5 years experience in an HR or People Operations role with a concentration on compensation and benefits
- Bachelor's degree in Mathematics, Business, or HR preferred
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Desire to work for a fast moving startup
- You share our [values](/handbook/values/), and work in accordance with those values
- The ability to work in a fast-paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#background-checks).
- Ability to use GitLab

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
- Next, candidates will be invited to a 45 minute interview with the Hiring Manager; Manager, Total Rewards
- Then, candidates will be invited to schedule a 45 minute interviews with the Manager, People Operations
- Final, candidates will be invited to schedule a 50 minute interview with the Senior Director, People Success

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).

## Senior Total Rewards Analyst

### Job Grade
The Senior Total Rewards Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Develop compensation strategies and principles.
- HRIS data entry, ensuring Data Integrity and alignment with all ancillary systems.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Maintain total rewards documentation.
- Benefits management.
- Collect and analyze data to track trends in functional areas.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Collaborate on training employees on various topics.
- Effectively communicate any upcoming total rewards processes or policy changes.
- Keep it efficient and DRY.

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)


## Manager, Total Rewards

### Job Grade

The Manager, Total Rewards is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Manage the Total Rewards Team (ICs):
  - Onboard, mentor, and grow the careers of all team members
  - Provide coaching to improve performance of team members and drive accountability
  - Plan, direct, supervise, and coordinate work activities of direct reports relating to compensation, benefits, and people data.
- Consistent and effective communication to all stakeholders to ensure alignment and clear understanding of priorities in line with company OKRs.
- Effectively communicate any upcoming total rewards processes or policy changes.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Coordinate Total Rewards data sync with Periscope to ensure accuracy in reporting monthly metrics.
- Manage and implement global compensation strategies and principles.
- Participate in Executive Compensation plan structures.
- Prepare materials for the Senior Manager to present at the Compensation Committee
- Global benefits management and development of benefit principles.
- Implement scalable processes, practices, and programs that support our rapid growth while continuing to deliver an incredible team member experience from direction of Total Rewards leadership.
- Manage the design and development of tools to assist employees in benefits selection, and to guide managers through compensation decisions.
- Mediate between benefits providers and employees, such as by assisting in handling employees' benefits-related questions or taking suggestions.
- Iterate based on data trend findings.
- Ensure compliance with all international rules and regulations.
- Audit all systems to ensure data accuracy.
- Manage Total Rewards documentation.
- Keep it efficient and DRY.

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)


## Senior Manager, Total Rewards

### Job Grade

The Senior Manager, Total Rewards is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Manage the Total Rewards Team (Managers/Leads & ICs)
  - Onboard, mentor, and grow the careers of all team members
  - Provide coaching to improve performance of team members and drive accountability
  - Responsible for budget costs/staffing of the group during annual planning
  - Establishes operational objectives and work plans and delegates assignments to directs.
- Work with Director, Total Rewards to shape a comprehensive global total rewards strategy that aligns with our culture and business plans, and allows GitLab to attract, retain, and motivate top talent.
- Manage the communications strategy and plan for total rewards, ensuring team members understand all that’s offered at GitLab.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Review Total Rewards data sync with Periscope to ensure accuracy in reporting monthly metrics.
- Develop recommendations for annual budgeting and planning while maintaining internal and external equity in pay plans; assist in management of annual compensation review cycles.
- Develop executive compensation; review, design, develop, and manage variable incentive pay for non-sales.
- Act as DRI from the People Group for the Compensation Committee meeting at GitLab: Work with Finance, Legal, & Senior Director People Success on agenda / supporting materials to facilitate meeting.
- Oversee benefits management
- Assist in management of stock administration and ensure accurate information is provided to team members about stock grants, vesting schedules, exercise process, and eligibility.
- Assist in management of equity planning and implementation of new schemes as needed; partner with Chief Financial Officer to manage the cap table.
- Assist in creation of scalable processes, practices, and programs that support our rapid growth while continuing to deliver an incredible team member experience.
- Manage and support the development, implementation, and maintenance or compensation tools and systems.
- Work with senior leadership and PBPs to understand compensation challenges and wins.
- Be open to feedback about compensation practices from the company and community to iterate based on findings.
- Ensure compliance with all international rules and regulations.
- Manage total rewards documentation.
- Keep it efficient and DRY.

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)


## Director, Total Rewards

The Director, Total Rewards leads the compensation and benefits function ensuring our total rewards strategy and practices continue to support our culture and growth. This role is a senior member of the People Group team who is responsible for building out the Total Rewards function and mentoring current team members. The Director, Total Rewards reports to the Chief People Officer.

### Job Grade
The Director, Total Rewards is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Lead and manage a growing Total Rewards team responsible for ensuring competitive compensation, benefits, and equity plans are in place.
  - Coach your managers to grow the careers of all team members
  - Lead the team to perform effective job evaluations/leveling, conduct market surveys, collect and analyze market data, and maintain compensation survey data; continuously updating our compensation ranges and job structure
  - Has overall control of budgetary planning and staffing strategy
- Design, develop, and execute on a comprehensive global total rewards strategy that aligns with our culture and business plans, and allows GitLab to attract, retain, and motivate top talent
- Own the communications strategy and plan for total rewards, ensuring team members understand all that’s offered at GitLab
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems. Oversee any large scale HRIS data projects.
- Manage Total Rewards data sync with Periscope to ensure accuracy in reporting monthly metrics
- Review and analyze GitLab’s total rewards (compensation, benefits, perks, recognition) practices globally and in relation to local markets; propose improvements to remain competitive
- Maintain expertise in industry practices and lead effective, competitive, and fair total rewards programs that ensure market consistency and cost-effectiveness
- Develop recommendations for annual budgeting and planning while maintaining internal and external equity in pay plans; manage annual compensation review cycles
- Work closely with the Chief People Officer to develop material for executive management and the Compensation Committee
- Develop and manage executive compensation; review, design, develop, and manage variable incentive pay for non-sales
- Oversee our benefits programs including insurance, retirement, time off, etc.
- Lead the team to perform effective job evaluations/leveling, conduct market surveys, collect and analyze market data, and maintain compensation survey data; continuously updating our compensation ranges and job structure
- Manage equity planning and implementation of new schemes as needed; partner with Chief Financial Officer to manage the cap table
- Create scalable processes, practices, and programs that support our rapid growth while continuing to deliver an incredible team member experience
- Oversee and support the development, implementation, and maintenance or compensation tools and systems
- Lead stock administration and ensure accurate information is provided to team members about stock grants, vesting schedules, exercise process, and eligibility
- Accurately interpret, counsel, communicate, and educate People Group members, managers, and executives on pay philosophy, policies, and practices
- Be open to feedback about compensation practices from the company and community to iterate based on findings
- Ensure compliance with all international rules and regulations.
- Manage total rewards documentation.
- Keep it efficient and DRY.

### Requirements
- Minimum 8 years of progressive experience in total rewards with in-depth knowledge of core compensation and job structure concepts and standard methodologies
- Experience designing and managing compensation and benefits programs, ideally at rapidly growing, global companies in a relevant industry
- Forward thinking, creative, and open-minded with sound technical skills, analytical ability, and seasoned judgment
- Comfortable and enthusiastic about working in a fast-paced, high growth, constantly changing, geographically dispersed, transparent environment
- Ability to drive consensus and engagement across a wide variety of stakeholders in multiple parts of the business
- Data-driven leader with a strong ability to analyze and turn data into insights and action plans aligned with company direction
- Excellent verbal and written communication skills, ability to package and present complex analyses and recommendations clearly
- Previous experience in both public and startup companies; IPO experience is ideal
- Experience preparing for and interacting with the Compensation Committee
- Experience working remotely and with remote team members is preferred

### Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)

### Career Ladder

The next step in the Total Rewards job family is to move to the [People Leadership](/job-families/people-ops/people-leadership/) job family. 

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
- Next, candidates will be invited to a 50 minute interview with the Hiring Manager
- Then, candidates will be invited to schedule three separate 45 minute interviews with the Senior Director, People Sucess and the Manager, Total Rewards and a People Business Partner
- Final candidates will be invited to conduct two separate 50 minute interview with the Chief Financial Officer and another Executive

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
