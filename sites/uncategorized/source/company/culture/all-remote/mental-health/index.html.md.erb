---
layout: handbook-page-toc
title: "Combating burnout, isolation, and anxiety in the remote workplace"
canonical_path: "/company/culture/all-remote/mental-health/"
description: Combating burnout, isolation, and anxiety in the remote workplace
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to recognize and avoid burnout in a remote setting. We'll also cover the issues of isolation and anxiety, and how to create a non-judgemental [culture](/company/culture/#culture-at-gitlab) where teams are encouraged to work through it rather than internalize it.

In this [CEO hanbook learning session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions), Sid and other leaders at GitLab discuss the importance of taking time off, managing burnout, and addressing imposter syndrome. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/od_KdZqc69k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Do not celebrate working long hours

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fH8nmtEoBh4?start=1579" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [People Group Conversation](https://youtu.be/fH8nmtEoBh4) above, GitLab CEO Sid Sijbrandij responds to a discussion on the topic of burnout and overwork.

> There's individual freedom, and there's peer pressure. As a company, we should take a lot of care that there's no peer pressure to work long hours.
>
> Everyone is used to that [being pressured]. At every company I've been at, that was a celebrated thing. We have to be super, super careful that we do not celebrate that at GitLab. — *Sid Sijbrandij, GitLab co-founder and CEO*

There's a fine line between [thanking someone publicly](/handbook/communication/#say-thanks) for going above and beyond to help out in a situation, and sending a message that work should always trump life.

Burnout rarely happens all at once. Rather, it typically takes one by surprise, eventually coming to a head after days, weeks, or months of overwork creep.

While working one additional hour to move a given project forward is likely not debilitating when viewed in a vacuum, it can trigger a revised baseline where you must *continue* to overwork in order to maintain the new status quo.

This becomes toxic when managers fail to recognize that a given sprint should not reset the baseline of what is achievable on an ongoing, sustained basis. It becomes disastrous when team members do not feel safe bringing this up to their managers in a [1:1 setting](/handbook/leadership/1-1/).

Particularly in a company where [results](/handbook/values/#results) are valued above all, managers should be careful to not assume that results garnered in a given period of overwork are the new norm. This places team members in an unfair scenario where they feel pressured to perpetually overwork in order to meet expectations. More broadly, as other team members witness this, they will be less likely to go above and beyond in special cases for fear of trapping themselves in a similar cycle of overworking just to meet ever-increasing (and unsustainable) expectations.

## Rest and time off are productive

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration-illustration.jpg){: .medium.center}

Though it sounds counter to conventional wisdom, clarity comes through time away from work. Just as a human must inhale and exhale to survive, one cannot expect to remain healthy and productive if only inhaling more work. 

As John Fitch describes in the book [Time Off](https://www.timeoffbook.com/), there are four stages of creativity. This was initially [outlined](https://www.brainpickings.org/2013/08/28/the-art-of-thought-graham-wallas-stages/) by English social psychologist and London School of Economics co-founder Graham Wallas.

1. Preparation
1. Incubation
1. Illumination
1. Verification

[John Fitch](https://twitter.com/johnwfitch), co-founder of [Time Off](https://www.timeoff.co/), articulates this on [The Culture Factor podcast](https://the-culture-factor.simplecast.com/episodes/what-if-time-off-created-a-culture-of-productivity-that-surpassed-12-hour-days). A portion of the interview is transcribed below.

> Incubation and illumination are only activated through time off — by not doing the actual work, by doing something else. While you're resting, parts of you are still working. I came to this contrarian belief that your best work is actually cultivated outside of work.
>
> 50% of the creative process requires you to *not* be working.  — *John Fitch*

Establishing a culture that gives voice to this reality is critical is removing the stigma from taking breaks and prioritizing wellbeing. Rest isn't at the expense of work; it's a core function of doing *excellent* work.

The L&D team hosted a [live speaker series](/handbook/people-group/learning-and-development/learning-initiatives/#learning-speaker-series-overview) with John Fitch to discuss how intential time off can help manage burnout and enable team members to create high quality work. Check out the recording below!

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BDvpoouM-us" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Wendy Barnes, Chief People Officer at GitLab, sat down with Samantha Lee from the [Learning and Development](/handbook/people-group/learning-and-development/) team for a [short conversation](https://youtu.be/MFowUT3GFDw) to answer the following questions.

1. What strategies can team members use to manage burnout?
1. How can managers support their team members in taking time off?
1. How can managers help prevent burnout?

Watch the short interview below.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oN8lzhQmFf4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Your rest ethic is as important as your work ethic

Traditionally, we've emphasized the importance of having a strong work ethic. Finding a balance with your work ethic and your rest ethic is essential for avoiding burnout. Building and nurturing your rest ethic creates the energy needed to enable your work ethic. Dr. Saundra Dalton Smith talks about the importance of rest and the types of rest we need in her TedTalk, [The 7 Types of Rest that Every Person Needs](https://ideas.ted.com/the-7-types-of-rest-that-every-person-needs/).

Examples of how you might define your rest ethic:

1. Commitment to taking [time off](/handbook/paid-time-off/) every month, and truly unplugging from your laptop or work projects when you're off
1. Using a [non-linear workday](/company/culture/all-remote/non-linear-workday/) to take a long break mid-day to walk, exercise, read, or work on a passion project
1. Practice a morning routine to help prepare you for the workday
1. Practice an evening routine to turn off work and be present with your friends, family, or time alone

To learn more about building a rest ethic, refer to this course by John Fitch and TimeOff entitled [Design your Rest Ethic](https://learn.timeoff.co/courses/time-off-design-your-rest-ethic).

GitLab hosted John Fitch for 2 Live [Speaker Series](/handbook/people-group/learning-and-development/learning-initiatives/#learning-speaker-series-overview) during the month of May 2021. In the following discussions, John shared great strategies for manager enablement of rest ethic, the importance of rest, and the impact that intentional rest has on results. Check out both recorded sessions below!

**Session 1**

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uklTuJeiTDo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Session 2**

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/acVRU5UjJEo?start=04" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Document processes around mental health

![GitLab commit illustration](/images/all-remote/gitlab-commit-illustration.jpg){: .medium.center}

Burnout, isolation, and anxiety are issues that impact team members across all companies, regardless of their organizational structure. While they aren't always intertwined, there is significant interplay between them.

In a colocated setting, it's entirely possible for a team member to *appear* well, but struggle with these issues internally. That said, it tends to be easier for those in an office to reach out to a team member they trust (or their [people department](/handbook/people-group/)) if burnout, isolation, or anxiety is impacting their ability to thrive in the workplace.

In a remote setting, where [in-person interactions](/company/culture/all-remote/in-person/) are less common, it's easier to fall victim to [isolation](/company/culture/all-remote/drawbacks/). This is particularly true for those who are not well acclimated to remote work, or have [just started their first remote role](/company/culture/all-remote/getting-started/).

Because you are likely to work alone most times, it's more difficult to remember that you **do** have colleagues to call on — especially if you're already overwhelmed, burned out, or suffering from anxiety/depression.

### Create clarity through documentation

The aforementioned reality makes it all the more important for any company hiring remote workers to place a great deal of focus on [documenting processes](/handbook/total-rewards/benefits/modern-health/#what-does-modern-health-offer) for team members who face these difficulties. Along with offering professional assistance (see [GitLab Modern Health](/handbook/total-rewards/benefits/#employee-assistance-program) as an example), be sure to showcase documented resources of where to turn during [onboarding](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members), and reinforce this in ongoing [learning and development](/company/culture/all-remote/learning-and-development/) sessions. 

Remote team members may feel less comfortable reaching out to a person when experiencing mental duress, so it's vital to ensure that answers and resources are easily discoverable within a company's handbook.

### Leverage documentation to ease anxiety

This approach enables managers and leaders to experience less anxiety and worry of being a single point of failure. By documenting diligently, it is easier for managers to take time for themselves, prioritize family, and earnestly disconnect during holidays and vacation. 

[Documentation](/company/culture/all-remote/handbook-first-documentation/) allows a significant portion of one's managerial expertise to be tapped into even while the manager is away recharging, and this intentional decentralization creates a greater sense of calm for both leaders and direct reports. 

[John Fitch](https://twitter.com/johnwfitch), co-author of [Time Off](https://www.timeoffbook.com/) and the Chief Product Officer at [Voltage Control](https://voltagecontrol.com/), shares a powerful anecdote on this approach on [The Culture Factor podcast](https://the-culture-factor.simplecast.com/episodes/what-if-time-off-created-a-culture-of-productivity-that-surpassed-12-hour-days). A portion of the interview is transcribed below.

> My function of head of design and prototyping — if I'm the only one doing it, that's a problem. If I go away or something happens to me, that entire function is compromised. It's a single point of failure. When you decentralize, you get rid of single points of failure. 
>
> Clarity is a part of a decentralized organization. That way, anyone can pick up and understand 'It's clear what we're here to do. Our values are clear.' 
>
> Much like an engineer will take their code and document it, that work is important. Another engineer can look at their code, and without emailing that person, make use of what has been created. Quality documentation has historically been a hallmark of engineering. We treat that the same regardless of function. When I'm away, I can be away. I don't have to worry about my team forgetting things.
>
> Just as journaling is important externally, it's powerful internally. Documenting my functions opens my eyes to self-improvement. As I'm writing things down, I can more clearly see better ways to do things. — *John Fitch*

## Creating a non-judgemental culture

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

[Transparency](/handbook/values/#transparency) is a core value at GitLab, and should be a value at any organization employing remote team members. Leaders should assume that some team members will feel uncomfortable surfacing issues involving isolation, burnout, and anxiety at work. This can stem from prior experiences, where bringing such issues to light could lead to negative consequences.

To combat this and destigmatize such issues, leadership should work to [build and sustain a non-judgemental culture](/company/culture/all-remote/building-culture/). This starts by celebrating a [diverse team](/handbook/values/#diversity-inclusion), and creating an [inclusive work environment](/company/culture/inclusion/).

At GitLab, we encourage team members to include overall feedback on how their life is going during [routine 1:1 meetings](/handbook/leadership/1-1/). Managers are responsible for creating a safe atmosphere, where team members can openly discuss issues related to mental health, and work with the team member to a resolution.

GitLab also offers a Slack channel — `#mental_health_aware` — dedicated to surfacing and discussing topics related to mental health.

### Force work into async tools

Working entirely or primarily in a chat tool such as Microsoft Teams or Slack is a pathway to burnout. Humans were not designed to have hundreds or thousands of people demanding things from them with red bubbles. There is a reason your phone can only allow one conversation at a time.

Leadership can create a more humane atmosphere by leaning on a tool (or tools) that enable asynchronous workflows, thereby [reducing meetings](/company/culture/all-remote/meetings/) and creating more time for focused, deep work. [GitLab uses GitLab](/solutions/gitlab-for-remote/) to accomplish this.

GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

## Workshop how to work less

![GitLab code illustration](/images/all-remote/gitlab-code-review.jpg){: .medium.center}

There should be no stigma in questioning [efficiency](/handbook/values/#efficiency). What was ultimately most efficient a year ago may not be true today. If you sense that a team is overworking and creating a cycle of overwork for those in proximity, consider pausing to workshop how to work *less*. 

This environment creates a space where individuals can surface new tools and technologies which may be able to lighten the human load, or surface new realities in how the market has shifted to a point where certain elements of work are no longer as valuable. 

A regular cycle of these workshops creates moments for reevaluation. A rest and creativity ethic is just as important as a work ethic, particularly when you consider that *outstanding* work requires a certain amount of creativity and clarity. 

## How to recognize mental health struggles

Oftentimes, if you are feeling burned out, you aren't the only one feeling that way. GitLab team members have compiled a list of symptoms related to burnout, isolation, and anxiety [in a blog post](/blog/2018/03/08/preventing-burnout/). A few are highlighted below.

1. You're constantly tired

1. You no longer enjoy things

1. Your job performance suffers

1. Your physical health suffers (headaches, irregular breathing patterns, etc.)

1. Your relationships are strained

1. You feel socially zapped

1. You disable video for team calls to prevent others from seeing your pain

1. You are perpetually concerned with whether you are doing enough

1. You worry that your contributions are too few or too insignificant

1. You feel unable to [choose family first](/handbook/values/#family-and-friends-first-work-second)

## Working to prevent burnout, isolation, and anxiety

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Q9yjo6IOqX4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the video above, Darren (Head of Remote, GitLab) and Sara (Senior Partner Marketing Manager, GitLab) discuss tips, tricks, and insights on preventing burnout and achieving balance.*

Prevention is a [team sport](https://www.cncf.io/blog/2020/04/03/were-all-in-this-together-a-wellness-guide-from-the-cncf-well-being-working-group/). Leaders must work to establish a workplace culture that empowers rather than restricts, managers must be proactive in sensing the signs of mental strain, and team members must feel comfortable surfacing issues while they are still manageable. Below are several [recommendations](/blog/2018/03/08/preventing-burnout/) for avoiding and preventing burnout, according to GitLab team members.

1. Set clear boundaries between work and home
1. Take vacation
1. Take a "mental health" day to lower your stress (spend time outdoors, get a massage, get some exercise)
1. Know when to take a break
1. Put a [break reminder](https://apps.apple.com/us/app/time-out-break-reminders/id402592703) on your computer
1. Switch off when you're away from work
1. Don't suffer in silence
1. Don't go straight to work after you wake up
1. Remove Slack from your smartphone or at the very least, turn off notifications for it
1. Keep each other accountable. When you notice someone in a different time zone should be asleep, tell them
1. Use your Slack status to share a message with the team that you are unavailable
1. Schedule [random coffee breaks](/handbook/communication/#random-room)
1. You can use [informal communication](/company/culture/all-remote/informal-communication/) mechanisms such as virtual trivia sessions, talent shows, scavenger hunts, and global pizza parties/meals to help bond as a team and prevent work-related burnout.

### The power of being proactive

GitLab has added a number of changes to the company [handbook](/handbook/), encouraging managers and team members to be proactive when it comes to recognizing and avoiding burnout, isolation, and anxiety.

1. [Encourage team members to communicate with their manager when they recognize burnout](/handbook/paid-time-off/#recognizing-burnout)
1. [Encourage team members to notice signs of burnout in their peers and direct reports](/handbook/paid-time-off/#recognizing-burnout)
1. [Added tips to avoid burnout](/handbook/paid-time-off/#recognizing-burnout)

### Take a moment for gratitude

It is easy to focus on the negatives. Many times when something goes wrong, or we make a mistake, we jump to the negative outcomes. In these moments, take a pause and ask yourself "What is good about this situation?". Focusing on the positive impact can make your day go a lot better. It is easy to forget to focus on gratitude, so if needed, help yourself with a prompt. A simple reminder in your calendar, or Post-It note at your work station with the word _gratitude_ can remind you to focus on what you've learned or discovered in a situation rather than the negatives. You can also try asking yourself what you are grateful for each morning to start getting in the habbit of focusing on the positives. 

This section has been contributed by [Matt Mochary](https://en.wikipedia.org/wiki/Matthew_Mochary).

GitLab team members are welcome to join the `#daily-gratitude` Slack channel (and remember, there's also a `:gratitude:` emoji!).

### Be transparent about boundaries

Whether you're communicating with a coworker or someone external to the company, it's helpful to be transparent about the boundaries you've set for your mental health, wellbeing, and life outside of work. This is especially impactful for managers and leaders, because it normalizes the conversation around mental health and sets an example for others on the team. 

If you're invited to a sync meeting that you can [contribute to asynchronously](/company/culture/all-remote/asynchronous/#how-to-decline-meetings-in-favor-of-async), this can be as simple as one line added to an email response. Here's an example from [Darren M.](https://gitlab.com/dmurph), GitLab's [Head of Remote](/company/culture/all-remote/head-of-remote/): 

> "I'm intentionally limiting my sync sessions in 2021 to [prioritize wellbeing and family](/handbook/values/#family-and-friends-first-work-second). Thanks for understanding."

This is also something you can discuss live with your manager or team during regular 1:1 or team meetings. Sharing openly about these boundaries builds empathy, trust, and reinforces a [non-judgmental culture](/company/culture/all-remote/mental-health/#creating-a-non-judgemental-culture).   

### Realistic expectations

Leadership must be sensible about expectations. If a company's [OKRs](/company/okrs/) (objectives and key results) and [KPIs](/handbook/ceo/kpis/) (key performance indicators) are unattainable without compromising company [values](/company/culture/all-remote/values/), this incongruence is a recipe for fostering burnout, isolation, and anxiety across a team.

It is foolish to expect a team member to maintain excellent mental health when their workload requires a sustained amount of sacrifice. There is a fine line between collaborating with a team member on an ambitious goal and assigning a task that will be perceived as impossible.

This nuance requires a leader who is adept at understanding a team member's strengths and weaknesses. What is perceived as impossible for one team member may seem trivial to another; it is not always the task that triggers duress, but mismatching a task with an ill-equipped team member.

This can be more pronounced in a remote setting. Leaders should pay close attention to blockers and struggles, and be proactive in asking about these during [1:1 sessions](/handbook/leadership/1-1/suggested-agenda-format/). Phrasing questions such as "Are there any assignments that you do not feel comfortable or equipped to handle?" is a better way to uncover truth compared to a blanket "Why isn't this working?"

It's also important to understand that not every team members prefers to discuss these topics using the same medium. While some may prefer video communication, others may prefer voice, writing, or something else. Remote leaders should strive to be [inclusive](/handbook/values/#diversity-inclusion) when searching for answers and solutions.

### Sentiment tracking and feedback

Particularly in remote companies, leadership should consider implementing processes around internal feedback. Companies will often wait to gather [internal feedback](/company/culture/internal-feedback/) until an exit interview after someone's resigned, or they'll organize an occasional survey to take a pulse on the company’s engagement. GitLab prefers shorter, but more frequent, check-ins, aligned to our values of [collaboration](/handbook/values/#collaboration) and [iteration](/handbook/values/#iteration).

Ask questions that shed light on whether or not a team member is thriving or struggling, and pay close attention to any adjustable workplace factors that are [contributing](/blog/2018/06/26/iterating-improving-frontend-culture/) either positively or negatively.

Learn more about [GitLab's approach and guidance on feedback](/handbook/people-group/guidance-on-feedback/).

## Mental Health Tool Stack

The following tools and strategies are used by the GitLab team to manage burnout and make mental health a priority.

| Tool | Use |
| ----- | ----- |
| [Headspace](https://www.headspace.com/subscriptions) | Meditation app |
| [Clockwise](https://www.getclockwise.com/) | Calendar management app |
| [Non-Linear Workdays](/company/culture/all-remote/non-linear-workday/) | All-remote work strategy to increase life harmony |
| README Files | Space to share bio, remote work setup, and information about working style. Check out the [GitLab's Engineering Team README files](/handbook/engineering/readmes/) as an example.|
| Slack | Non-work conversation in channels like `#mindfulness` and `#mental-health-aware` |
| [The donut bot](/company/culture/all-remote/informal-communication/#the-donut-bot) | Random coffee chat meeting match bot |
| [Modern Health Community Circles](https://circles.modernhealth.com/) | Public community converstaions hosted by Modern Health |
| [Coffee Chats](/company/culture/all-remote/informal-communication/#coffee-chats) | Social calls with team members |
| [Walk and Talk meeting structure](/handbook/communication/#walk-and-talk-calls) | An optional structure for both work and social calls |
| [Burnout Index](https://burnoutindex.org/) | Burnout assessment tool |
| [Burnout assessment](https://ssir.org/pdf/2005WI_Feature_Maslach_Leiter.pdf) | Burnout assessment tool |
| [Daylio](https://daylio.net/) |  Mood tracker app |

## Additional resources

1. [UCSF Department of Psychiatry and Behavioral Sciences: Resources to support your mental health during the COVID-19 outbreak and climate crises](https://psych.ucsf.edu/copingresources) 

### LinkedIn Learning Courses 

1. [Mindful Stress Management](https://www.linkedin.com/learning/mindful-stress-management/introduction-to-stress-management?u=2255073)
1. [Winding Down: Get a Better Nights Sleep](https://www.linkedin.com/learning/winding-down-get-a-better-night-s-sleep/introduction-the-importance-of-sleep?u=2255073)
1. [How to Create a Life of Meaning and Purpose](https://www.linkedin.com/learning/how-to-create-a-life-of-meaning-and-purpose/creating-a-meaningful-life-is-easier-than-you-think?u=2255073)

## GitLab Knowledge Assessment: Combating burnout, isolation, and anxiety in the remote workplace

Complete all knowledge assessments in the [Remote Work Foundation certification](/company/culture/all-remote/remote-certification/) to receive the [Remote Foundations Badge in GitLab Learn](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.


## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## Contribute your lessons

Creating a healthy remote workplace is essential to business success. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
